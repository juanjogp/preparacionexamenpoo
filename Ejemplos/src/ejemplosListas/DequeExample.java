package ejemplosListas;

import java.util.*; 

public class DequeExample 
{ 
	public static void main(String[] args) 
	{ 
		Deque<String> deque = new LinkedList<String>(); 

		// Formas de a�adir elementos en un deque
		deque.add("Element 1 (Tail)"); // a�adir al final (cola)
		deque.addFirst("Element 2 (Head)"); //a�adir al principio
		deque.addLast("Element 3 (Tail)"); // a�adir al final
		deque.push("Element 4 (Head)"); //a�adir al principio (pila)
		deque.offer("Element 5 (Tail)"); // a�adir al final
		deque.offerFirst("Element 6 (Head)"); //a�adir al principio
		deque.offerLast("Element 7 (Tail)"); // a�adir al final

		System.out.println(deque + "\n"); 

		// Iterar con los elementos. 
		System.out.println("Standard Iterator"); 
		Iterator iterator = deque.iterator(); 
		while (iterator.hasNext()) 
			System.out.println("\t" + iterator.next()); 


		// Iterador reverso
		Iterator reverse = deque.descendingIterator(); //descendingIterator() lo pone al reves
		System.out.println("Reverse Iterator"); 
		while (reverse.hasNext()) 
			System.out.println("\t" + reverse.next()); 

		// Peek devuelve el principio sin eliminarlo del deque
		System.out.println("Peek " + deque.peek()); 
		System.out.println("After peek: " + deque); 

		// Pop devuelve el principio y elimina el elemento del deque
		System.out.println("Pop " + deque.pop()); 
		System.out.println("After pop: " + deque); 

		// Comprovar si elemento X existe en el deque (contains) 
		System.out.println("Contains element 3: " + 
						deque.contains("Element 3 (Tail)")); 

		// Eliminar Primer elemento 
		deque.removeFirst(); 
		// Eliminar Ultimo elemento 
		deque.removeLast(); 
		System.out.println("Deque after removing " + 
							"first and last: " + deque); 

	} 
} 
