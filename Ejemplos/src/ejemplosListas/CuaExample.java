package ejemplosListas;

import java.util.ArrayDeque;
import java.util.Deque;

public class CuaExample { //El ultimo en entrar es ultimo en salir
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<String> pila = new ArrayDeque<>();
		
		pila.add("Element 1"); //addLast
		pila.add("Element 2");
		pila.add("Element 3");
		System.out.println("Eliminar primer elemento introducido "+pila.remove());
		pila.add("Element 4");
		pila.add("Element 5");
		System.out.println("Eliminar segundo elemento introducido "+pila.remove());
		pila.add("Element 6");

		while (!pila.isEmpty())
			System.out.println(pila.pop());
	}
}
