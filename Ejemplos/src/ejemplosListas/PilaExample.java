package ejemplosListas;

import java.util.ArrayDeque;
import java.util.Deque;

public class PilaExample { //El ultimo en entrar es primero en salir
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<String> pila = new ArrayDeque<>();

		pila.push("Element 1"); //addfirst
		pila.push("Element 2");
		pila.push("Element 3");
		System.out.println("Eliminar ultimo elemento introducido "+pila.pop()); // pop = remove first
		pila.push("Element 4");
		pila.push("Element 5");
		System.out.println("Eliminar ultimo elemento introducido "+pila.pop());
		pila.push("Element 6");

		while (!pila.isEmpty())
			System.out.println(pila.pop());
	}
}
