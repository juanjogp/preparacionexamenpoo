package ejemploInterface;

public class ClasseImplenta implements InterfaceEjemplo {

	@Override
	public void metodo1() {
		System.out.println("Este metodo esta implementado 1");
	}

	@Override
	public void metodo2() {
		System.out.println("Este metodo esta implementado 2");
	}

}
