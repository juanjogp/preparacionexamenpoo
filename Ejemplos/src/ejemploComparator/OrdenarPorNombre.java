package ejemploComparator;

import java.util.Comparator;

class OrdenarPorNombre implements Comparator<Student> 
{ 
    // Orden ascendente por nombre
    public int compare(Student a, Student b) 
    { 
        return a.name.compareTo(b.name); 
    } 
} 
