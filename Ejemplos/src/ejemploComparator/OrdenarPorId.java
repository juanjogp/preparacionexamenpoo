package ejemploComparator;

import java.util.Comparator;

class OrdenarPorId implements Comparator<Student> {
	// orden ascendiente por id
	public int compare(Student a, Student b) {
		return a.id - b.id;
	}
}
