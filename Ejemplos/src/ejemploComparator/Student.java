package ejemploComparator;

class Student 
{ 
    int id; 
    String name, address; 
  
    // Constructor 
    public Student(int rollno, String name, 
                               String address) 
    { 
        this.id = rollno; 
        this.name = name; 
        this.address = address; 
    } 
  
    // Used to print student details in main() 
    public String toString() 
    { 
        return this.id + " " + this.name + 
                           " " + this.address; 
    } 
} 
