package ejemploIterator;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorExample {

	public static void main(String[] args) {
		ArrayList<Integer> test = new ArrayList<Integer>();
		test.add(1);
		test.add(3);
		test.add(5);
		test.add(7);
		test.add(9);
		
		Iterator<Integer> itTest = test.iterator(); //Crear Iterator de nuestro array
		
		while(itTest.hasNext()) { //Recorrer todo el Array
			int testNum = itTest.next();
			System.out.println(testNum);
		}

	}

}
