package ejemploSobrecarga;

public class Boi {
	private String nom;
	private int edat;
	private boolean esBoi;
	
	public Boi(String nom, int edat, boolean esBoi) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.esBoi = esBoi;
	}
	
	//Este tiene sobrecarga
	public Boi(String nom, int edat) {
		super();
		this.nom = nom;
		this.edat = edat;
		this.esBoi = false;
	}
	
}
